package com.sgatu.mbt;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MBTWorker implements Runnable {
    private final LinkedList<Object> messages = new LinkedList<>();
    private final LinkedList<Object> pendingMessages = new LinkedList<>();
    private final Object messagesLock = new Object();
    private final Object pendingMessagesLock = new Object();
    private AtomicBoolean running = new AtomicBoolean(false);
    private MBTSystem system;
    private long timeout = -1L;
    private long lastProcessedMessage = 0L;
    private long lastLoop = 0L;
    private long started = 0L;
    void setSystem(MBTSystem system) {
        this.system = system;
    }
    void setTimeout(final long timeout) { this.timeout = timeout; }
    protected MBTSystem getSystem(){
        return system;
    }

    protected abstract void processMessage(Object message) throws Throwable;
    protected void beforeStart() { }
    protected void beforeStop() { }
    protected void stop() {
        running.set(false);
    }
    public boolean isRunning(){
        return running.get();
    }
    private void loop() {
        running.set(true);
        lastLoop = lastProcessedMessage = System.currentTimeMillis();
        beforeStart();
        started = System.currentTimeMillis();
        while (running.get()) {
            if(timeout > -1 && (started+timeout) < System.currentTimeMillis()){
                running.set(false);
                break;
            }
            lastLoop = System.currentTimeMillis();
            Object message = null;
            synchronized (messagesLock) {
                if(messages.size() > 0){
                    message = messages.removeFirst();
                }
                synchronized (pendingMessagesLock) {
                    if(pendingMessages.size() > 0){

                        Iterator<Object> it = pendingMessages.iterator();
                        while(it.hasNext()){
                            Object aux = it.next();
                            messages.add(aux);
                            it.remove();
                        }
                    }
                }
            }
            if(message != null){
                try {
                    processMessage(message);
                }catch (Throwable e){
                    e.printStackTrace();
                }
                lastProcessedMessage = System.currentTimeMillis();
            } else {
                try {
                    Thread.sleep(5);
                }catch (InterruptedException e){
                    e.printStackTrace();
                    running.set(false);
                }
            }
        }
        beforeStop();
    }
    protected void beforeNewMessage(Object message) {}
    public void tell(Object message) {
        synchronized (pendingMessagesLock) {
            try {
                beforeNewMessage(message);
            }catch (Exception e){ e.printStackTrace(); }
            pendingMessages.add(message);
        }
    }

    public void run() {
        loop();
    }
    public void restart() {
        getSystem().newWorker(this);
    }

    public long getLastProcessedMessage() {
        return lastProcessedMessage;
    }

    public long getLastLoop() {
        return lastLoop;
    }
}
