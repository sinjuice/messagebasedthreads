package com.sgatu.mbt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SchedulerWorker extends MBTWorker {
    HashMap<Long, List<ScheduledMessage>> messages = new HashMap<Long, List<ScheduledMessage>>();
    boolean firstMessage = false;
    protected void processMessage(Object message) throws Throwable {
        ScheduledMessage rescheduleMessage = null;
        if(message instanceof ScheduledMessage) {
            queueMessage((ScheduledMessage) message);
            if(((ScheduledMessage) message).getPeriod() > 0 && ((ScheduledMessage) message).getWorker().isRunning()) {
                ((ScheduledMessage) message).setNextRun(System.currentTimeMillis() + ((ScheduledMessage) message).getPeriod());
                rescheduleMessage = ((ScheduledMessage) message);
            }
            if(!firstMessage) {
                firstMessage = true;
                tell(SchedulerLoopStep.getInstance());
            }
        } else if (message instanceof SchedulerLoopStep){
            long messageGroup = System.currentTimeMillis()/60000L;
            if(messages.containsKey(messageGroup)){
                Iterator<ScheduledMessage> msgsIt = messages.get(messageGroup).iterator();
                while(msgsIt.hasNext()){
                    ScheduledMessage sch_message = msgsIt.next();
                    if(sch_message.getNextRun() <= System.currentTimeMillis()){
                        sch_message.getWorker().tell(sch_message.getMessage());
                        msgsIt.remove();
                        if(sch_message.getPeriod() > 0 && sch_message.getWorker().isRunning()) {
                            sch_message.setNextRun(System.currentTimeMillis() + sch_message.getPeriod());
                            rescheduleMessage = sch_message;
                        }
                    }
                }
                if(messages.get(messageGroup).size() == 0){
                    messages.remove(messageGroup);
                }
            }
            tell(SchedulerLoopStep.getInstance());
        }
        if(rescheduleMessage != null) {
            queueMessage(rescheduleMessage);
        }
    }


    private void queueMessage(ScheduledMessage msg){
        long messageGroup = msg.getNextRun()/60000L;
        if(!messages.containsKey(messageGroup)){
            messages.put(messageGroup, new ArrayList<>());
        }
        if(msg.getNextRun() <= System.currentTimeMillis()){
            msg.getWorker().tell(msg.getMessage());
        } else {
            messages.get(messageGroup).add(msg);
        }
    }
}
