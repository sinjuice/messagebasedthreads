package com.sgatu.mbt;

class ScheduledMessage {
    private Object message;
    private long nextRun;
    private long period;
    private MBTWorker worker;

    public ScheduledMessage(final Object message, final long nextRun, final long period, final MBTWorker worker) {
        this.message = message;
        this.nextRun = nextRun;
        this.period = period;
        this.worker = worker;
    }

    public Object getMessage() {
        return message;
    }

    public long getNextRun() {
        return nextRun;
    }

    public void setNextRun(final long nextRun) {
        this.nextRun = nextRun;
    }

    public MBTWorker getWorker() {
        return worker;
    }

    public long getPeriod() {
        return period;
    }
}
