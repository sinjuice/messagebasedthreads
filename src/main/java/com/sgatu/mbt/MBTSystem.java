package com.sgatu.mbt;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class MBTSystem {
    private MBTWorker schedulerWorker;
    private ThreadPoolExecutor finiteWorkersPool;
    public MBTSystem(int finiteWorkersPoolSize) {
        schedulerWorker = newWorker(new SchedulerWorker());
        finiteWorkersPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(finiteWorkersPoolSize);
    }
    public MBTSystem(){
        this(100);
    }
    public MBTWorker newWorker(final MBTWorker worker){
        worker.setSystem(this);
        Thread thr = new Thread(worker);
        thr.start();
        return worker;
    }
    public MBTWorker newFiniteWorker(final MBTWorker worker){
        worker.setSystem(this);
        worker.setTimeout(-1);
        Future<?> f = finiteWorkersPool.submit(worker);
        return worker;
    }
    public MBTWorker newFiniteWorker(final MBTWorker worker, final long timeout){
        worker.setSystem(this);
        worker.setTimeout(timeout);
        finiteWorkersPool.submit(worker);
        return worker;
    }
    public void schedule(Object message, MBTWorker worker, long when){
        schedulerWorker.tell(new ScheduledMessage(message, when, -1, worker));
    }
    public void schedule(Object message, MBTWorker worker, long when, long period){
        schedulerWorker.tell(new ScheduledMessage(message, when, period, worker));
    }
    public void shutdown() {
        schedulerWorker.stop();
        finiteWorkersPool.shutdownNow();
    }
}
